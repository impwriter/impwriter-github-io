---
layout: post
title:  "Cara Memasang Meta Tag Twitter Card dengan Benar"
author: Ilham Maulana Pratama
categories: [ SEO, tutorial ]
image: https://1.bp.blogspot.com/-VAReMLH9kgs/YOKogrxHu4I/AAAAAAAABUQ/cIddzYy6aYYmYitkFnzucMHcMwmCy7PCgCLcBGAsYHQ/s0/20210705_133532_0001.jpg
tags: [SEO]
---

<p>Salah satu cara mendapat backlink gratis dan berkualitas adalah dengan membagikan postingan artikel ke sosial media, salah satunya adalah di Twitter.</p><p>Sebenarnya untuk hanya membagikan link di Twitter kamu hanya perlu mengkopi pastekan link tersebut dan mempostingnya. Namun ada beberapa trik untuk dapat menampilkan hasil postingan link berupa Thumbnail dan Deskripsi, Link Aplikasi, serta video. Caranya adalah menggunakan Meta Tag Twitter Card.</p><p>Ada beberapa jenis meta tag Twitter, diantaranya adalah:</p><p></p><ul style="text-align: left;"><li>Summary Large Image (Menampilkan Gambar Thumbnail serta Deskripsi.)</li><li>Apps (Menampilkan Logo Aplikasi)</li><li>Video (Menampilkan Thumbnail Video)</li></ul><p>Namun kali ini saya akan membagikan cara untuk menampilkan Thumbnail dan Deskripsi artikel di Twitter. Yaitu dengan Menambahkan Meta Tag Twitter Card Summary Large Image di website kamu.</p>

<p>Hasil tampilan postingan twitter setelah menggunakan Summary with Large Image adalah seperti berikut ini:</p>
<blockquote class="twitter-tweet"><p dir="ltr" lang="en">Happy 3rd anniversary <a href="https://twitter.com/hashtag/TBT?src=hash&amp;ref_src=twsrc%5Etfw">#TBT</a>! See how "Throwback Thursday" cemented its status as a weekly Twitter tradition: <a href="https://t.co/IhGdmShWH6">https://t.co/IhGdmShWH6</a></p>— Twitter (@Twitter) <a href="https://twitter.com/Twitter/status/593828669740584960?ref_src=twsrc%5Etfw">April 30, 2015</a></blockquote> <script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js"></script>

<p>Caranya menerapkannya cukup mudah, kamu hanya perlu mengcopy code di bawah ini, kemudian menaruhnya di html directory website kamu.</p><p>Masukan kode diantara &lt;head&gt; dan &lt;/head&gt;</p>
<p>Untuk pengguna CMS Blogger Kamu bisa menggunakan code dibawah ini:</p>

<pre style="background: black; color: white;"><code>
&lt;!--[ Twitter Card ]--&gt;

    &lt;meta expr:content='data:blog.pageName.escaped ? data:blog.pageName.escaped : data:blog.title.escaped' name='twitter:title'/&gt;

    &lt;meta expr:content='data:blog.canonicalUrl' name='twitter:url'/&gt;

    &lt;b:if cond='data:view.isMultipleItems'&gt;

      &lt;b:if cond='data:blog.metaDescription'&gt;

        &lt;meta expr:content='data:blog.metaDescription.escaped' name='twitter:description'/&gt;         

        &lt;b:else/&gt;

        &lt;meta expr:content='data:blog.pageName.escaped ? data:blog.pageName.escaped : data:blog.title' name='twitter:description'/&gt;

      &lt;/b:if&gt;

     &lt;b:else/&gt;

      &lt;b:if cond='data:blog.metaDescription'&gt;

        &lt;meta expr:content='data:blog.metaDescription.escaped' name='twitter:description'/&gt;            

        &lt;b:else/&gt;

        &lt;meta expr:content='data:post.snippet.escaped snippet { length: 147, links: false, linebreaks: false, ellipsis: false }' name='twitter:description'/&gt;

      &lt;/b:if&gt;

    &lt;/b:if&gt;
    
    &lt;meta content='summary_large_image' name='twitter:card'/&gt;

    &lt;meta expr:content='data:blog.pageName.escaped ? data:blog.pageName.escaped : data:blog.title.escaped' name='twitter:image:alt'/&gt;

    &lt;b:if cond='data:blog.postImageUrl'&gt;

      &lt;meta expr:content='resizeImage(data:blog.postImageUrl, 0)' name='twitter:image:src'/&gt;

      &lt;b:else/&gt;

      &lt;meta content='<span class="block">Add_your_image_url_here'</span> name='twitter:image:src'/&gt;

    &lt;/b:if&gt;
</code></pre>

<br />
<p>Kode <span style="background: rgb(43, 0, 254); color: white;">Add_your_image_url_here</span> Ganti dengan link gambar apapun, gunanya untuk menampilkan gambar dari url tersebut jika thumbnail postingan artikel tidak tampil atau terdeteksi.</p><p>Bagi para pengguna WordPress caranya juga sangat mudah, kamu hanya tinggal meng-install plugin toast SEO, selain untuk mengatur metatag SEO dan Markup pada website, plugin tersebut juga dapat menampilkan thumbnail gambar pada postingan Twitter nantinya.</p><p>Itu dia sedikit penjelasan tentang cara memasang meta tag Twitter Card dengan benar. Jika kamu masih ada pertanyaan prihal artikel ini, jangan sungkan untuk menanyakannya di kolom komentar. Sekian Terimakasih.</p><p>Penulis: Ilham Maulana</p>
